import { Machine, actions, sendParent } from "xstate";
const { assign } = actions;

export const cartMachine = Machine({
  id: "cart",
  initial: "empty",
  context: {
    id: undefined,
    title: "",
    prevTitle: ""
  },
  on: {
    TOGGLE_ADD_TOO_CART: {
      target: ".reading.completed",
      actions: [
        assign({ completed: true }),
        sendParent(ctx => ({ type: "TODO.COMMIT", todo: ctx }))
      ]
    },
    TOGGLE_REMOVE_FROM_CART: "deleted"
  },
  states: {
    reading: {
      initial: "unknown",
      states: {
        unknown: {
          on: {
            "": [
              { target: "completed", cond: ctx => ctx.completed },
              { target: "pending" }
            ]
          }
        },
        pending: {
          on: {
            SET_COMPLETED: {
              target: "completed",
              actions: [
                assign({ completed: true }),
                sendParent(ctx => ({ type: "TODO.COMMIT", todo: ctx }))
              ]
            }
          }
        },
        completed: {
          on: {
            TOGGLE_COMPLETE: {
              target: "pending",
              actions: [
                assign({ completed: false }),
                sendParent(ctx => ({ type: "TODO.COMMIT", todo: ctx }))
              ]
            },
            SET_ACTIVE: {
              target: "pending",
              actions: [
                assign({ completed: false }),
                sendParent(ctx => ({ type: "TODO.COMMIT", todo: ctx }))
              ]
            }
          }
        },
      },
    },
  }
});
