import React, {useState} from 'react';

import Frontpage from './pages/frontpage'
import CartTotalSection from './partials/CartTotalSection';
import PersonalDetails from './partials/personalDetails';
import PaymentDetails from './partials/paymentDetails';

function App () {
  const [progressIndex, setProgressindex] = useState(0)
  const [count, setCount] = useState(1)
  const [name, setName] = useState('')
  const [email, setEmail] = useState('')
  const [card, setCard] = useState(null)
    var content =
      <div className="mt-3 container">
        <Frontpage count={count} setCount={setCount}></Frontpage>
        {progressIndex > 0 ?
         <PersonalDetails name={name} setName={setName} setEmail={setEmail} email={email}></PersonalDetails>
        : ''}

        {progressIndex > 1 ?
         <PaymentDetails card={card} setCard={setCard}></PaymentDetails>
        : ''}

        <CartTotalSection count={count} progressIndex={progressIndex} setProgressindex={setProgressindex}></CartTotalSection>
      </div>

    return content

}

export default App;