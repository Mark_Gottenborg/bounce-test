import React, {useState} from 'react';
import Product from '../partials/product';

function FrontPage (props) {
  return (
    <div>
      <div className="d-flex flex-column justify-content-start align-items-start">
        <small>booking storage at:</small>
        <h1 className="h4">Cody's Cookie Store</h1>
      </div>
      <div>
        <Product count={props.count} setCount={props.setCount}></Product>
      </div>
    </div>
  );
}

export default FrontPage;