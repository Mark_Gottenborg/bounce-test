import React from 'react';

function PersonalDetails (props) {
  return (
    <div className="d-flex flex-column justify-content-start align-items-center">
      <h2 className="h6">Personal Details:</h2>
      <label htmlFor="name">Name:</label>
      <input type="text" onChange={() => {props.setName( event.target.value)}} name="name" id="name"/>
      <label htmlFor="email">Email:</label>
      <input type="email" onChange={() => {props.setEmail( event.target.value)}} name="email" id="email"/>
    </div>
  );
}

export default PersonalDetails;