import React, {useState} from 'react';

function CartTotalSection (props) {
  return (
    <div className="container fixed-bottom d-flex flex-row justify-content-between align-items-center">
      <div className="d-flex flex-column justify-content-start align-items-center">
        <small>{props.count} bag(s)</small>
        <span>$ {props.count * 5.90}</span>
      </div>
      <button onClick={() => props.setProgressindex(props.progressIndex + 1)} className="btn btn-primary">Next</button>
    </div>
  );
}

export default CartTotalSection;