import React, { useState } from 'react';

function Product(props) {
  return (
    <div>
      <div className="d-flex flex-row justify-content-between align-items-center">
        <label className="text-nowrap" htmlFor="noOfBags">Number of bags:</label>
        <div className="d-flex flex-row justify-content-center align-items-center">
          <button onClick={() => props.setCount(props.count - 1)} disabled={props.count <= 1 ? true : false} className="btn">-</button>
          <span className="col-2">{props.count}</span>
          <button onClick={() => props.setCount(props.count + 1)} disabled={props.count >= 2 ? true : false} className="btn">+</button>
        </div>
      </div>
    </div>
  )
}

export default Product;