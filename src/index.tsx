
import React from 'react';
import ReactDOM from 'react-dom';

import { useMachine } from '@xstate/react';
import { Machine } from 'xstate';

import App from './components/App';

ReactDOM.render(<App />, document.getElementById('root'));